public class Main {
    public static void main(String[] args) {
        Human one = new Human("Ivanov", "Ivan", "Ivanovich");
        Human two = new Human("Petrov", "Oleg");

        System.out.println(one.getFullName());
        System.out.println(one.getShortName());
        System.out.println(two.getFullName());
        System.out.println(two.getShortName());
        System.out.println(one);
        System.out.println(two);
    }
}
