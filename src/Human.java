public class Human {
    private String surname;
    private String middleName;
    private String name;

    public Human(String surname, String name) {
        this.surname = surname;
        this.name = name;
    }

    public Human(String surname, String name, String middleName) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = surname + " " + name;
        if (middleName != null) {
            result += " " + middleName;
        }
        return result;
    }

    public String getFullName() {
        return toString();
    }

    public String getShortName() {
        String result = surname + " " + name.charAt(0) + ".";
        if (middleName != null) {
            result += middleName.charAt(0) + ".";
        }
        return result;
    }
}
